# Copyright 2022 Matthieu Jacquemet
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .openmedia_utils import env_get_int, env_get_str
import os


API_METHODS         = {"GET", "POST", "PUT", "DELETE"}
# SERVER_NAME         = env_get_str("SERVER_NAME")
LOG_FILE            = env_get_str("OPENMEDIA_LOG_FILE")
LOG_LEVEL           = env_get_str("OPENMEDIA_LOG_LEVEL", "NOTSET")
HOST                = env_get_str("HOST", "0.0.0.0")
PORT                = env_get_int("PORT", 8080)
PROVIDERS           = {"module://providers/ygg"}
ALL_CONTENT         = {"movie", "show", "game"}