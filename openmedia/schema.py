# Copyright 2022 timus
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import strawberry
import typing

from . import database

from strawberry.types import Info
from strawberry.scalars import JSON

@strawberry.type
class MediaContent:
    name: str
    size: int
    data: JSON


@strawberry.type
class Query:

    @strawberry.field
    def contents(self, info: Info) -> typing.List[MediaContent]:
        print(info.context["request"])
        return []
