# Copyright 2022 Matthieu Jacquemet
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from lib2to3.pgen2.pgen import ParserGenerator
from os import environ
from urllib import response

from pyutils.misc import Singleton, override
from flask import Flask
from argparse import ArgumentParser
from sys import argv
from click import echo

class OpenMedia(Singleton, Flask):

    def __init__(self) -> None:
        Flask.__init__(self, "scraproxy")

        parser = ArgumentParser()
        parser.add_argument('--config', type=str, nargs=1, help='Path to config file')
    
        self.load_config(parser.parse_args(argv[1:]))


    def load_config(self, options, defaults="scraproxy.default_settings"):

        self.config.from_object(defaults)

        if options.config is not None:
            self.config.from_pyfile(options.config)

        elif "SCRAPROXY_SETTINGS" in environ:
            self.config.from_envvar('SCRAPROXY_SETTINGS')


    def load_providers(self):
        


app = OpenMedia.instance()