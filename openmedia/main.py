# Copyright 2022 timus
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from fastapi import FastAPI, Query
import os
import logging

from deta import Deta
import strawberry

from . import schema
from . import legacy_managers
from .content_manager import ContentManager

from strawberry.asgi import GraphQL
from strawberry.fastapi import GraphQLRouter

try:
    from deta import app as deta_app

    cron = deta_app.lib.cron
except Exception:
    cron = lambda *args : lambda fn : fn


logger = logging.Logger("app")

app = FastAPI()
deta = Deta()

graphql_router = GraphQLRouter(strawberry.Schema(schema.Query))

app.include_router(graphql_router, prefix="/v1")

@cron()
def cron_job(event):
    ContentManager.run_jobs(event)
