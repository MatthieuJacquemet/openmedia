# Copyright 2022 Matthieu Jacquemet
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pyutils.path import join_module_path
from pyutils.config import get_int, get_bool, get_list
from typing import Callable, Iterable, List
from os import environ

def module_path(path) -> List[str]:
    return join_module_path(path)

def env_get_int(key: str, default: int = 0) -> int:
    return get_int(environ.get(key, default))

def env_get_str(key: str, default: str = "") -> str:
    return environ.get(key, default)

def apply(func: Callable, collection: Iterable) -> None:
    list(map(func, collection))