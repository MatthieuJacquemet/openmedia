# Copyright 2022 timus
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import aiohttp

from sqlalchemy import over
from .content_manager import ContentProvider, ContentManager
from . import schema

import importlib
import urllib
import sys, os
from typing import List
from pyutils.misc import override



class ModuleManager(ContentManager, id="module"):

    @override
    def get_provider(self, uri: str) -> ContentProvider:
        """
        Load the provider specified in the module passed as parameter.
        The module must define a variable named "provider" in its root scope.
        This variable must be an instance of ContentProvider.
        """
        if not uri.startswith("/"):
            uri = os.path.join(os.path.dirname(__file__), uri)

        if not os.path.isdir(uri):
            raise self.ProviderException(f"Could not find {uri}")

        provider_dir: str = os.path.dirname(uri)
        provider_name: str = os.path.basename(uri)

        if provider_dir not in sys.path:
            sys.path.insert(0, provider_dir)

        try:
            module = importlib.import_module(provider_name)
            return module.provider
        except Exception as e:
            raise self.ProviderException(f"failled to load module: {e}")



class NetworkManager(ContentManager, id=["http", "https"]):

    def __init__(self, scheme: str):
        self._scheme: str = scheme

    class NetworkProvider(ContentProvider):
        
        def __init__(self, authority: str):
            self._session = aiohttp.ClientSession(base_url=authority)
        
        @override
        async def get_content(self, type: str, id: int, max: int, size: int
                            ) -> List[schema.MediaContent]:
            data = self._send_request(  f"/content/{type}",
                                        id=id, max=max, size=size)

        
        async def _send_request(self, path: str, **params) -> dict:
            url = f"{path}?{urllib.parse.urlencode(params)}"

            async with self._session.get(url) as response:
                return await response.json()

    @override
    def get_provider(self, uri: str) -> ContentProvider:
        if not uri:
            raise self.ProviderException(f"Invalid provider url")

        return self.NetworkProvider(self._scheme + uri)