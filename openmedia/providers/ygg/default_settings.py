
TRACKER_HIDE_COMPLETE       = True
TRACKER_TIMEOUT             = 5
TRACKER_MAX_RETRY           = -1
TRACKER_INTERVAL            = 0

YGG_USERNAME                = ""
YGG_PASSWORD                = ""
YGG_SEARCH_ORIGINAL_TITLE   = True
YGG_MAX_TORRENT             = 4

DOMAIN_TIMEOUT   = 86400 # 24h
YGGSCRAPER_LOGIN_INTERVAL   = "1:00:00"
YGGSCRAPER_TIMEOUT          = 30

SCRAP_ITEMS                 = [
                                {
                                    "name": "films_&_videos",
                                    "subcategories": [ {"name": "film"} ],
                                },
                                {
                                    "name": "films_&_videos",
                                    "subcategories": [ {"name": "serie_tv"} ],
                                },
                                {
                                    "name": "films_&_videos",
                                    "subcategories": [ {"name": "animation"} ],
                                },
                                {
                                    "name": "films_&_videos",
                                    "subcategories": [ {"name": "animation_serie"} ],
                                },
                                {
                                    "name": "films_&_videos",
                                    "subcategories": [ {"name": "documentaire"} ],
                                },
                            ]

REQUEST_TIMEOUT             = 10.0
DB_URI                      = 