# Copyright 2022 timus
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re
import os
import time
import datetime
import copy

from threading import Timer
from types import NoneType
from bs4 import BeautifulSoup, Tag
from unidecode import unidecode
from http import HTTPStatus as status, cookiejar

from .categories import categories
from . import patterns
from . import default_settings as settings

import collections
collections.Callable = collections.abc.Callable

import requests
from requests import Response, sessions, Session

from .parse import PTN
from typing import Callable, Iterator, Tuple, List, Iterator, Union, NoReturn

from pyutils.misc import ClassUtils, override


YGG_DOMAIN              = "yggtorrent.fi"
YGG_FULL_DOMAIN         = f"www5.{YGG_DOMAIN}"

YGG_BASE_URL            = f"https://{YGG_FULL_DOMAIN}"
YGG_LOGIN_URL           = f"{YGG_BASE_URL}/user/login"
YGG_LOGOUT_URL          = f"{YGG_BASE_URL}/user/logout?attempt=1"
YGG_SEARCH_URL          = f"{YGG_BASE_URL}/engine/search?"
YGG_SETTING_URL         = f"{YGG_BASE_URL}/user/settings"

YGG_TOKEN_COOKIE        = "ygg_"

YGG_GET_FILES           = f"{YGG_BASE_URL}/engine/get_files?torrent="
YGG_GET_INFO            = f"{YGG_BASE_URL}/engine/get_nfo?torrent="
YGG_GET_TORRENT         = f"{YGG_BASE_URL}/engine/download_torrent?id="

YGG_MOST_COMPLETED_URL  = f"{YGG_BASE_URL}/engine/mostcompleted"
YGG_NON_CONNECTED       = bytes("Vous devez vous connecter pour télécharger un torrent", "utf-8")

TORRENT_PER_PAGE = 50


def extract_domain(url: str) -> str:

    match = re.search(r"https?://(.+\.)?(yggtorrent\.[^/]+)/?", url)
    if match:
        return match.group(2)

    return YGG_DOMAIN


def load_domain() -> str:
    try:
        resp = requests.get("https://yggland.fr/FAQ-Tutos/")
    except Exception:
        return YGG_DOMAIN

    page = BeautifulSoup(resp.content, features="html.parser")
    
    if tag := page.select_one("#table-tester > tbody > tr > td"):
        return extract_domain(tag.text)


def find_first(items: list[dict], name: str, key: str="name") -> str:

    for item in items:
        if item.get(key) == name:
            return item

    return None


def build_query_string(args: dict) -> str:

    def build_args():
        for key, value in args:
            yield f"{key}={value.replace(' ','+')}"

    return "&".join(build_args())


def get_options(section: list[dict], data: dict) -> list[Tuple[str, str]]:

    ret = list()

    for key, values in data.get("options",()).items():

        option = find_first(section, key)
        if option is None:
            continue

        for value in values:
            try:
                index = option["values"].index(value)
            except ValueError:
                continue
                
            name = "option_" + option['name']
            if "multiple" in option:
                name += "%3Amultiple"

            ret.append((name + "%5B%5D", str(index+1)))
        
    return ret


def create_query_string(context: list[dict], **options) -> list[Tuple[str, str]]:
    args = list()

    for key, value in options.items():
        entry = find_first(context, key)

        args.append((key, "+".join(value) if type(value) is list else value))

        category = find_first(categories, data["category"])
        
        args.append(("category", category["id"]))

        sub = category["subcategories"]

        if "subcategory" in data:

            subcategory = find_first(sub, data["subcategory"])
            args.append(("sub_category", subcategory["id"]))

            section = subcategory["options"]
            args.extend(get_options(section, data))

        elif data["category"] == "films_&_videos":

            options = data.get("options", {})

            if options.keys() <= {"langue", "qualite"}:
                section = sub[0]["options"]
                args.extend(get_options(section, data))

    args.append(("do","search"))

    return args


def create_search_url(**options) -> str:

    args = create_query_string(**options)
    return YGG_SEARCH_URL + build_query_string(args)


def is_full_season(name: str) -> bool:

    name = unidecode(name).lower()

    return (re.match(r"[ _\-\.]+s(eason|aison)?[ _\-\.]+[0-9]+[ _\-\.]+",name)
        and not re.match(r"[ _\-\.]+e(pisode)?[ _\-\.]+[0-9]+[ _\-\.]+",name))


def to_bytes(size: str) -> int:

    value, unit = float(size[:-2]), size[-2]

    if unit == "G":     value *= 10**9
    elif unit == "M":   value *= 10**6
    elif unit == "k":   value *= 10**3

    return int(value)


def get_episode_from_options(data: dict) -> int:

    options = data.get("options")

    if options:
        episode = options.get("episode")
        if episode and episode[0].startswith("episode"):
            return int(episode[0][-2:])


def get_torrent_id(link: str) -> int:

    match = re.match("/([0-9]+)-", link)
    if match:
        return int(match.group(1))


def is_video(filename: str) -> bool:
    _, ext = os.path.splitext(filename)
    return ext.lower() in {".mkv", ".mp4", ".avi", ".flv", ".mov", ".wmv"}


def t_to_sec(value: str) -> int:

    if not isinstance(value, str):
        return value

    t = time.strptime(value, "%H:%M:%S")

    return datetime.timedelta(hours=t.tm_hour,
                              minutes=t.tm_min,
                              seconds=t.tm_sec).total_seconds()


def time_str_to_sec(value: str) -> int:
    return datetime.datetime.strptime(value, r"%d/%m/%Y %H:%M").total_seconds()


def get_subcat_name_from_id(id: str):

    for category in categories:
        if subcat := find_first(category["subcategories"], id, "id"):
            return subcat["name"]
    
    return None



class Torrent(ClassUtils):

    def __init__(self, data: BeautifulSoup) -> None:
        super().__init__()
        self.load_data(data)

    @classmethod
    def _get_details(cls, torrent_name: str) -> dict:
        return PTN(torrent_name).parts

    def load_data(self, data: BeautifulSoup) -> None:
        
        info    = data.select("#informationsContainer tbody tr")
        stats   = data.select("#adv_search_cat > td")
        id_tag  = data.select_one("#report-torrent input[name='target']")

        lines   = info.find_all("tr", recursive=False)

        subcat          = lines[2].find("div")["class"]
        self.type       = re.match(r"tag_subcat_(\d)", subcat).group(1)
        self.name       = lines[0][1].text.strip()
        self.url        = lines[1].find("a")["href"]
        self.id         = id_tag["value"]
        self.timestamp  = time_str_to_sec(lines[6][1].text.strip())
        self.size       = to_bytes(lines[3][1].text)
        self.seeders    = int(stats[1][0].text)
        self.leechers   = int(stats[3][0].text)
        self.completed  = int(stats[5][0].text)
        self.details    = self._get_details(self.name)
        

    def iter_files(self, session=None):

        session = session or requests
        response = session.get(YGG_GET_FILES + self.id)

        data = response.content.decode("unicode-escape", "ignore")
        files_page = BeautifulSoup(data, features="html.parser")

        return map(lambda tag: TorrentFile(self, tag), files_page.find_all("tr"))


    def find_episode_file(self, episode_num, session=None):

        if not "episode" in self.details:
            for file in self.iter_files(session):

                if is_video(file.filename):
                    _, episode = file.get_show_info()

                    if episode == episode_num:
                        return file



class TorrentEntry(Torrent):

    def __init__(self, data: BeautifulSoup) -> None:
        super().__init__(data)


    @override
    def load_data(self, data: BeautifulSoup) -> None:

        tds = data.find_all("td", recursive=False)
        
        self.type       = tds[0].find("div").text.strip()
        self.name       = tds[1].find("a").text.strip()
        self.url        = tds[1].find("a")["href"]
        self.id         = tds[2].find("a")["target"]
        self.timestamp  = tds[4].find("div").text.strip()
        self.size       = to_bytes(tds[5].text)
        self.completed  = int(tds[6].text)
        self.seeders    = int(tds[7].text)
        self.leechers   = int(tds[8].text)
        self.details    = self._get_details(self.name)




class TorrentFile:

    def __init__(self, torrent: Torrent, file_tag: Tag):
        tds = file_tag.find_all("td")

        self.torrent = torrent
        self.size = to_bytes(tds[0].text.strip())
        self.filename = tds[1].text.strip()


    def get_show_info(self):

        season = None
        episode = None

        for pattern_name, pattern in patterns.patterns:

            if pattern_name in ("season", "episode"):
                match = re.search(pattern, self.filename, re.I)
                if not match:
                    continue

                data = int(match.group(2))
                if pattern_name == "season":
                    season = data
                elif pattern_name == "episode":
                    episode = data
        
        return season, episode



class YGG_Scraper:

    class ScraperError(Exception): pass


    def __init__(   self, domain: str=YGG_DOMAIN, username: str=None,
                    password: str=None, session_token: str=None):

        self._session = Session()
        self._logged_in = False
        self._username = username
        self._password = password

        self.set_domain(domain or load_domain())
        self.store_session_cookie(session_token)
            

    def store_session_cookie(self, session_token: str) -> None:
        if session_token:
            self._session.cookies.set(  value   = session_token,
                                        domain  = YGG_DOMAIN,
                                        name    = YGG_TOKEN_COOKIE)


    def retrieve_session_token(self, renew: bool=True) -> str:
        
        if token := self._session.cookies.get(YGG_TOKEN_COOKIE):
            return token
        elif renew:
            response = self._session.get(YGG_BASE_URL)
            token = response.cookies.get(YGG_TOKEN_COOKIE)

        return token


    def _ensure_logged_in(self) -> bool:
        
        if not self._username or not self._password:
            return False
        
        elif not self._logged_in and self.retrieve_session_token():

            response = self._session.post(YGG_LOGIN_URL,
                data = {"id":   self._username,
                        "pass": self._password,}
            )
            self._logged_in = response.status_code == status.OK

        return self._logged_in


    def logout(self) -> bool:

        response = self._session.get(YGG_LOGOUT_URL)
        self._logged_in = response.status_code != status.OK

        return not self._logged_in


    def find_pages(self, options: dict) -> list[Tag, List[Tag]]:

        first_page = self._get_page(create_search_url(**options))
        pagination = first_page.find("ul", {"class": "pagination"})

        if pagination is not None:
            others = [tag["href"] for tag in pagination.find_all("a")[1:]]
        else:
            others = list()
        
        return first_page, others


    def download_torrent(self, id: int, retry: bool=True) -> bytes:

        if self._ensure_logged_in():
            response = self._session.get(YGG_GET_TORRENT + str(id))

            if response.content != YGG_NON_CONNECTED:
                return response.content
            else:
                self._logged_in = False
                self._session.cookies.set(YGG_TOKEN_COOKIE, None)
                if retry:
                    return self.download_torrent(id, False)

        raise self.ScraperError("Must be connected to download torrent")


    def get_announce_url(self) -> Union[str, NoneType]:
        
        if self._ensure_logged_in():
            page = self._get_page(YGG_SETTING_URL)

            url = page.select_one("#update-settings > table > tr > td + td")
            if url is not None:
                return url.text.strip()

        raise self.ScraperError("Must be logged to get announce url")


    def _get_page(self, url: str) -> BeautifulSoup:
        response = self._session.get(url)
        return BeautifulSoup(response.content, features="html.parser")


    def list_torrents(self, page: Tag) -> Iterator[TorrentEntry, None, None]:

        yield from map( lambda tag: TorrentEntry(tag.parent.parent),
                        page.find_all("a", {"id": "torrent_name"}))


    def iter_all_torrents(self, options: map) -> Iterator[TorrentEntry, None, None]:

        first_page, others = self.find_pages(options)
        yield from self.list_torrents(first_page)

        for page_url in others:
            yield from self.list_torrents(self._get_page(page_url))


    def get_total_torrent_count(self) -> Union[int, NoneType]:
        page = self._get_page(create_search_url())

        item = page.select_one("#\#torrents a[target]")
        if item:
            return int(item["target"])

        raise self.ScraperError("Failed to get number of torrents")


    @classmethod
    def set_domain(cls, domain: str) -> None:

        global  YGG_DOMAIN, YGG_FULL_DOMAIN, YGG_BASE_URL, YGG_LOGIN_URL, \
                YGG_LOGOUT_URL, YGG_SEARCH_URL, YGG_GET_FILES, YGG_GET_INFO, \
                YGG_GET_TORRENT, YGG_MOST_COMPLETED_URL, YGG_SETTING_URL

        YGG_DOMAIN              = domain
        YGG_FULL_DOMAIN         = f"www5.{YGG_DOMAIN}"
        YGG_BASE_URL            = f"https://{YGG_FULL_DOMAIN}"

        YGG_LOGIN_URL           = f"{YGG_BASE_URL}/user/login"
        YGG_LOGOUT_URL          = f"{YGG_BASE_URL}/user/logout?attempt=1"

        YGG_SEARCH_URL          = f"{YGG_BASE_URL}/engine/search?"
        YGG_GET_FILES           = f"{YGG_BASE_URL}/engine/get_files?torrent="
        YGG_GET_INFO            = f"{YGG_BASE_URL}/engine/get_nfo?torrent="
        YGG_GET_TORRENT         = f"{YGG_BASE_URL}/engine/download_torrent?id="
        YGG_SETTING_URL         = f"{YGG_BASE_URL}/user/settings"

        YGG_MOST_COMPLETED_URL  = f"{YGG_BASE_URL}/engine/mostcompleted"