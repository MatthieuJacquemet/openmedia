# Copyright 2022 timus
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from email.generator import Generator
import logging


from openmedia.content_manager import ContentProvider, override
from openmedia.openmedia_utils import apply
from openmedia import app, deta
from openmedia import schema

from .scraper import YGG_Scraper, load_domain, Torrent
from typing import List, Generator
import typing
import random

from . import default_settings as settings


logger = logging.Logger("ygg-torrent")


class YggProvider(ContentProvider):

    def __init__(self):
        super().__init__("ygg", priority=10)
        self._db = deta.Base("ygg")
        self._scraper = YGG_Scraper(self.ensure_domain_up_to_date(),
                                    self.get_session_token(),
                                    settings.YGG_USERNAME,
                                    settings.YGG_PASSWORD)


    def get_session_token(self) -> str:
        return self._db.get("session_token")["value"]

    def __del__(self):
        session_token = self._scraper.retrieve_session_token(False)
        self._db.put(session_token, "session_token")


    @override
    def get_content(self, ctype: str, id: int, max: int, size: int
                    ) -> List[schema.MediaContent]:
        return []


    @override
    def run_job(self, event) -> None:
        self._scraper.set_domain(self.ensure_domain_up_to_date())

        apply(self._fetch_content, random.shuffle(self._get_missing_ids()))

        

    def _fetch_content(self, content_id: int) -> None:
        try:
            torrent = self._scraper.get_torrent(content_id)
        except YGG_Scraper.ScraperError as e:
            logger.error(f"Could not get torrent info: {e}")
            return

        self._db.update({"value": self._db.util.append(content_id)}, "all_ids")
        self._db.put(self._create_entry(torrent), str(torrent.id))


    def _create_entry(self, torrent: Torrent) -> dict:
        
        return {
            "id":           torrent.id,
            "name":         torrent.name,
            "type":         torrent.type,
            "timestamp":    torrent.timestamp,
            "size":         torrent.size,
            "seeders":      torrent.seeders,
            "leechers":     torrent.leechers,
            "completed":    torrent.completed,
            "details":      torrent.details,
        }


    def ensure_domain_up_to_date(self) -> str:

        if (domain := self._db.get("domain")) is None:
            domain = load_domain()
            self._db.put(domain, "domain", expire_in=settings.DOMAIN_TIMEOUT)

        return domain


    def _get_missing_ids(self) -> typing.Generator[int, None, None]:
        try:
            num_ids = self._scraper.get_total_torrent_count() + 1
        except Exception as e:
            logger.error(f"cannot get missing ids: {e}")
            return

        tags = list(zip([True] * num_ids, range(num_ids)))

        if (already_in := self._db.get("all_ids")) is None:
            already_in = self._db.put(list(), "all_ids")

        for already_in_id in already_in["value"]:
            tags[already_in_id][0] = False
        
        yield from map(lambda p: p[1], filter(lambda x: x[0], tags))


provider = YggProvider()