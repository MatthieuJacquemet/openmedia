# Copyright 2022 timus
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio

from sqlalchemy.ext.asyncio import create_async_engine
from . import default_settings as settings

class YggDataBase:

    def __init__(self):
        self._engine = create_async_engine(settings.DB_URI, echo=True)