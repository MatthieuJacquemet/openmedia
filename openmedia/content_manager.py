# Copyright 2022 timus
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from abc import abstractmethod
import os
import logging
import bisect

from numpy import partition
from . import schema
from . import default_settings as settings
from pyutils.misc import ClassRegistry, ClassUtils, override, Singleton
from typing import Dict, Generator, List, Set

logger = logging.Logger("provider-manager")



class ContentProvider(ClassUtils):

    def __init__(self, name: str, contents: List[str] = settings.ALL_CONTENT,
                priority: int = 0):
        self._name = name
        self._content_types = contents
        self._priority = priority

    @abstractmethod
    async def get_content(self, type: str, id: int, max: int, size: int
                            ) -> List[schema.MediaContent]:
        """
        This method must implement the logic that fetch media content and fill
        the database with that content.
        This method is run by default on a daily basis.
        """
        pass
    

    async def run_job(self, event) -> None:
        """
        This method must implement the logic to that must be run on  daily basis
        This could be for instance fetching new content to other database and
        putting it on a local cache.
        """
        pass

    @property
    def name(self) -> str:
        return self._name

    @property
    def content_types(self) -> List[str]:
        return self._content_types

    @property
    def priority(self) -> int:
        return self._priority



class ContentManager(metaclass=ClassRegistry, registry=True):

    class ProviderException(Exception): pass

    _providers: Dict[str, List[ContentProvider]] = dict()


    @classmethod
    def load_providers(cls) -> Dict[str, List[ContentProvider]]:
        """
        Load all the providers defined in the config variable "PROVIDERS"
        """
        cls._managers = {k:v(k) for k, v in cls._DATABASE.items()}

        for provider_uri in settings.PROVIDERS:
            try:
                cls._load_provider(provider_uri)
            except ContentManager.ProviderException as e:
                logger.error(f"Failed to load provider: {e}")

        return cls._providers


    @classmethod
    def _load_provider(cls, manager_uri: str) -> ContentProvider:
        """
        Load a single content provider matching the uri passed as parameter.
        This function may raise a ProviderException if the provider cannot
        be created
        The return value is the provider created
        """
        scheme, _, path = manager_uri.partition("://")

        if (manager := cls._managers.get(scheme)) is not None:
            provider = manager.get_provider(path)

            if provider is None:
                raise cls.ProviderException(f"No provider for {manager_uri}")
        else:
            raise cls.ProviderException(f"No manager for {scheme}")

        for ctype in provider.content_types:
            bisect.insort(  cls._providers.setdefault(ctype, list()),
                            provider,
                            key=lambda x: x.priority)
        
        logger.info(f"Loaded provider {provider.name}")
        return provider


    @classmethod
    def run_jobs(cls, event) -> None:
        providers: Set[ContentProvider] = set()
    
        for provider_list in cls._providers.values():
            providers.update(provider_list)
        
        list(map(lambda provider: provider.run_job(event), providers))


    def __init__(self, scheme: str = ""):
        pass


    @abstractmethod
    def get_provider(self, uri: str) -> ContentProvider:
        """
        This method must return the content provider assotiated with the uri
        passed as parameter.
        """
        pass
    
